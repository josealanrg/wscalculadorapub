package miCalculadora;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Calculadora c=new Calculadora();
		
		System.out.println("CALCULADORA ARITMETICA");
		System.out.println("======================");

		System.out.println("Sumando 2+3 = "+c.suma(2, 3));
		System.out.println("Restando 10-4 = "+c.resta(10, 4));
		
		System.out.println("Multiplicando 4*3 = "+c.multiplica(4, 3));
		System.out.println("Dividiendo 4/2 = "+c.divide(4, 2));
	}

}
