package miTest;

import static org.junit.Assert.assertTrue;
import miCalculadora.Calculadora;

import org.junit.Before;
import org.junit.Test;

public class TestMultiplicaYDivide {
	Calculadora c;

	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void testMultiplica() {
		assertTrue(c.multiplica(3, 4)==12);
	}

	@Test
	public void testDivide() {
		assertTrue(c.divide(4, 2)==2);
	}

}
