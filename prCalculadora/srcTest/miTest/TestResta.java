package miTest;

import static org.junit.Assert.*;
import miCalculadora.Calculadora;

import org.junit.Before;
import org.junit.Test;

public class TestResta {
	Calculadora c;

	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void testResta() {
		assertTrue(c.resta(5,3)==2);
	}

}
